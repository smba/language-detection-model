# language-detection-model

## Run web app locally
```bash
pip install -r requirements.txt

# if streamlit is on $PATH
streamlit run app.py  

# else
python -m streamlit run app.py
```
